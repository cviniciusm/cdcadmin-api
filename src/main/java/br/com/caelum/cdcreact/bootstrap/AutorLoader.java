package br.com.caelum.cdcreact.bootstrap;

import java.math.BigDecimal;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import br.com.caelum.cdcreact.daos.AutorDao;
import br.com.caelum.cdcreact.daos.LivroDao;
import br.com.caelum.cdcreact.models.Autor;
import br.com.caelum.cdcreact.models.Livro;

@Profile("dev")
@Component
public class AutorLoader implements CommandLineRunner {

	private final AutorDao autorDao;
	
	private final LivroDao livroDao;

	public AutorLoader(AutorDao autorDao, LivroDao livroDao) {
		super();
		this.autorDao = autorDao;
		this.livroDao = livroDao;
	}

	@Override
	public void run(String... args) throws Exception {
		Livro livro;

		Autor autor1 = Autor.builder().nome("alberto").email("alots@gmail.com").senha("123456").build();
		autorDao.save(autor1);
		
		Autor autor2 = Autor.builder().nome("gabriel").email("gabriel@gmail.com").senha("789012").build();
		autorDao.save(autor2);
		
		Autor autor3 = Autor.builder().nome("joao").email("joao@gmail.com").senha("654321").build();
		autorDao.save(autor3);

		livro = Livro.builder().titulo("Livro 01").autor(autor1).preco(new BigDecimal("29.99")).build();
		livroDao.save(livro);

		livro = Livro.builder().titulo("Livro 02").autor(autor1).preco(new BigDecimal("30.45")).build();
		livroDao.save(livro);
		livro = Livro.builder().titulo("Livro 02").autor(autor2).preco(new BigDecimal("30.45")).build();
		livroDao.save(livro);
		
		livro = Livro.builder().titulo("Livro 03").autor(autor1).preco(new BigDecimal("30.45")).build();
		livroDao.save(livro);
		livro = Livro.builder().titulo("Livro 03").autor(autor2).preco(new BigDecimal("30.45")).build();
		livroDao.save(livro);
		livro = Livro.builder().titulo("Livro 03").autor(autor3).preco(new BigDecimal("30.45")).build();
		livroDao.save(livro);
	}

}

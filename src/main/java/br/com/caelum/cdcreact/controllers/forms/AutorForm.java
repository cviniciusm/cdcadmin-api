package br.com.caelum.cdcreact.controllers.forms;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import br.com.caelum.cdcreact.models.Autor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AutorForm {

	@NotBlank
	private String nome;

	@NotBlank
	@Email
	private String email;

	@NotBlank
	private String senha;

	public Autor build() {
		Autor autor = Autor.builder()
			.email(email)
			.nome(nome)
			.senha(senha)
			.build();

		return autor;
	}

}

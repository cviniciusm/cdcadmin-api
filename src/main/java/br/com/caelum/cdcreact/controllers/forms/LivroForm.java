package br.com.caelum.cdcreact.controllers.forms;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.caelum.cdcreact.daos.AutorDao;
import br.com.caelum.cdcreact.models.Livro;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LivroForm {

	@NotBlank
	private String titulo;
	
	@NotNull
	private BigDecimal preco;
	
	@NotNull
	private Integer autorId;
	
	public Livro build(AutorDao autorDao){
		return new Livro(titulo, preco, autorDao.findById(autorId).get());
	}
	
}

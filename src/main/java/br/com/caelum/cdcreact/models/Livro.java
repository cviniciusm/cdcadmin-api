package br.com.caelum.cdcreact.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Livro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(value = AccessLevel.NONE)
	private Integer id;
	
	@NotBlank
	private String titulo;
	
	@NotNull
	private BigDecimal preco;
	
	@ManyToOne
	@NotNull
	private Autor autor;

	public Livro(@NotBlank String titulo, @NotNull BigDecimal preco, @NotNull Autor autor) {
		this.titulo = titulo;
		this.preco = preco;
		this.autor = autor;
	}

}
